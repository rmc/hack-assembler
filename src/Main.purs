module Main where

import Prelude

import Control.Monad.Eff.Console (log, CONSOLE)
import Node.FS (FS)
import Node.Process (PROCESS)
import Data.Array (fromFoldable)
import Data.File (readFile, writeFile)
import Data.List (List)
import Data.Parser (parse, userVars, labelVars, program, Token)
import Data.SymbolTable (SymbolTable, buildVariableSymbols, newSymbolTable, predefinedSymbols)
import Data.Transformer (transform)
import Data.Either (Either, either)
import Text.Parsing.StringParser (ParseError)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Exception (EXCEPTION, error, throwException)
import Data.Traversable (traverse, sequence)

main :: ∀ eff. Eff ( console ∷ CONSOLE , exception ∷ EXCEPTION , fs ∷ FS, process ∷ PROCESS | eff ) Unit
main = do
  let
    vars = \tokens -> buildVariableSymbols (fromFoldable $ userVars tokens)
    labels = \tokens -> fromFoldable $ labelVars tokens
    createTable = \tokens -> newSymbolTable [predefinedSymbols, (labels tokens), (vars tokens)]

  log "reading file"
  src <- readFile "./prog.asm"

  log "parsing"
  let
    tokens :: Either ParseError (List Token)
    tokens = parse src
  log $ show tokens

  log "creating symbol table"
  let
    table :: Either ParseError (SymbolTable)
    table = createTable <$> tokens
  log $ show table

  log "transforming"
  trans <- traverse sequence (map <$> (transform <$> table) <*> (program <$> tokens))
  log $ show $ trans

  log "writing file"
  _ <- sequence (writeFile "prog.hack" <$> trans)
  log "finish"
