module Data.SymbolTable where

import Prelude

import Data.Array (length, zip, (..), nub)
import Data.Either (Either(..))
import Data.Foldable (foldl)
import Data.StrMap (StrMap, insert, empty, lookup)
import Data.String.Regex (Regex, regex, test)
import Data.String.Regex.Flags (noFlags)
import Data.Tuple (Tuple(..), uncurry)
import Partial.Unsafe (unsafePartial)
import Data.Maybe (Maybe)

type Address = Int
type Symbol' = String

type SymbolTable = StrMap Address

insertTuple :: SymbolTable → Tuple Symbol' Address → SymbolTable
insertTuple = flip (uncurry insert)

batchInsert :: SymbolTable -> Array (Tuple Symbol' Address) -> SymbolTable
batchInsert = foldl insertTuple

lookupSymbol :: SymbolTable -> Symbol' -> Maybe Address
lookupSymbol = flip lookup

newSymbolTable :: Array (Array (Tuple Symbol' Address)) -> SymbolTable
newSymbolTable ss = foldl batchInsert empty ss

predefinedSymbols :: Array (Tuple Symbol' Address)
predefinedSymbols =
  [ Tuple "SP" 0
  , Tuple "LCL" 1
  , Tuple "ARG" 2
  , Tuple "THIS" 3
  , Tuple "THAT" 4
  , Tuple "SCREEN" 16384 -- 0x4000
  , Tuple "KBD"  24576 -- 0x6000
  ] <> registerSymbols

registerSymbols :: Array (Tuple Symbol' Address)
registerSymbols = map (\i -> Tuple ("R" <> show i) i) (0 .. 15)

registerSymbolRegex :: Regex
registerSymbolRegex = unsafePartial case regex "^R[1-9][1-5]?$" noFlags of
    Right r -> r

isVariable :: Symbol' -> Boolean
isVariable "SP" = false
isVariable "LCL" = false
isVariable "ARG" = false
isVariable "THIS" = false
isVariable "THAT" = false
isVariable "SCREEN" = false
isVariable "KBD" = false
isVariable s = not $ test registerSymbolRegex s

variableSymbolInitialAddress :: Address
variableSymbolInitialAddress = 16

buildVariableSymbols' :: Address -> Array Symbol' -> Array (Tuple Symbol' Address)
buildVariableSymbols' from variables = zip uniqueVars addresses where
  uniqueVars =  nub variables
  addresses = (from .. (from + (length uniqueVars) - 1))

buildVariableSymbols :: Array Symbol' -> Array (Tuple Symbol' Address)
buildVariableSymbols = buildVariableSymbols' variableSymbolInitialAddress

-- TODO: check duplicate symbols
-- validateVariables :: Array Symbol ->

-- labels :: Array Symbol -> SymbolTable
-- labels symbols =
