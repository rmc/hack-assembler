module Data.Transformer where

import Prelude

import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Exception (EXCEPTION, error, throwException)
import Data.Int (toStringAs, binary, fromStringAs, decimal)
import Data.Maybe (maybe)
import Data.Parser (AInstructionArg(..), CInstructionArg(..), Token(..))
import Data.String (length)
import Data.SymbolTable (SymbolTable, lookupSymbol)

pad :: (String -> String) -> Int -> String -> String
pad p n s
  | length s < n = pad p n (p s)
  | otherwise = s

address :: ∀ eff. String → Eff ( exception ∷ EXCEPTION | eff ) String
address a = a
  # fromStringAs decimal
  # maybe handleNothing handleValue where
    handleNothing = throwException $ error ("invalid address: " <> a)
    handleValue n = pure $ (pad ((<>) "0") 16 $ toStringAs binary n)

transform :: ∀ eff. SymbolTable -> Token -> Eff ( exception :: EXCEPTION | eff ) String
transform _ (A (AddressA addr)) = address addr
transform st (A (SymbolA symbol)) = symbol
  # lookupSymbol st
  # maybe handleNothing handleValue where
    handleNothing = throwException $ error ("symbol not found")
    handleValue = (map ((pad ((<>) "0") 16))) <<< address <<< show
transform st (C (CompJumpC cmp jmp)) = pure $ "111" <> comp cmp <> dest "" <> jump jmp
transform st (C (DestCompC dst cmp)) = pure $ "111" <> comp cmp <> dest dst <> jump ""
transform st (C (DestCompJumpC dst cmp jmp)) = pure $ "111" <> comp cmp <> dest dst <> jump jmp

transform _ _ = pure ""

comp :: String -> String
comp "0" = "0101010"
comp "1" = "0111111"
comp "-1" = "0111010"
comp "D" = "0001100"
comp "A" = "0110000"
comp "M" = "1110000"
comp "!D" = "0001101"
comp "!A" = "0110001"
comp "!M" = "1110001"
comp "-D" = "0001111"
comp "-A" = "0110011"
comp "-M" = "1110001"
comp "D+1" = "0011111"
comp "A+1" = "0110111"
comp "M+1" = "1110111"
comp "D-1" = "0001110"
comp "A-1" = "0110010"
comp "M-1" = "1110010"
comp "D+A" = "0000010"
comp "D+M" = "1000010"
comp "D-A" = "0010011"
comp "D-M" = "1010011"
comp "A-D" = "0000111"
comp "M-D" = "1000111"
comp "D&A" = "0000000"
comp "D&M" = "1000000"
comp "D|A" = "0010101"
comp _ = "1010101" -- "D|M"

jump :: String -> String
jump "JGT" = "001"
jump "JEQ" = "010"
jump "JGE" = "011"
jump "JLT" = "100"
jump "JNE" = "101"
jump "JLT" = "110"
jump "JMP" = "111"
jump _ = "000"

dest :: String -> String
dest "M" = "001"
dest "D" = "010"
dest "MD" = "011"
dest "A" = "100"
dest "AM" = "101"
dest "AD" = "110"
dest "AMD" = "111"
dest _ = "000"
