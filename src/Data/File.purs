module Data.File where

import Prelude

import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Exception (EXCEPTION)
import Data.Foldable (foldMap)
import Node.Encoding (Encoding(..))
import Node.FS (FS)
import Node.FS.Sync (readTextFile, writeTextFile)
import Node.Path (FilePath, concat)
import Node.Process (PROCESS, cwd)
import Data.List (List)

readFile :: ∀ eff. FilePath → Eff ( fs ∷ FS , exception ∷ EXCEPTION | eff ) String
readFile path = do
  contents <- readTextFile UTF8 path
  pure $ contents

getDestFilePath :: ∀ eff. String → Eff ( process ∷ PROCESS | eff ) String
getDestFilePath relativePath = do
  workdir <- cwd
  pure $ concat [workdir, relativePath]

newLine :: String
newLine = "\n"

writeFile :: ∀ eff. FilePath → List String ->  Eff ( process ∷ PROCESS, fs ∷ FS , exception ∷ EXCEPTION | eff ) Unit
writeFile path lines = do
  dest <- getDestFilePath path
  writeTextFile UTF8 dest $ foldMap (_ <> newLine) lines
