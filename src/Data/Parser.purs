module Data.Parser where

import Prelude

import Data.List (List(..), foldl, filter, (:), reverse)
import Data.String (singleton)
import Data.Traversable (traverse)
import Text.Parsing.StringParser (Parser, runParser, ParseError, try)
import Text.Parsing.StringParser.Combinators as C
import Text.Parsing.StringParser.String as S
import Control.Alt ((<|>))
import Data.Either (Either, isRight)
import Data.Tuple (Tuple(..), fst)

type LineNumber = Int
type LineContents = String
newtype Line = Line LineContents
type Lines = List Line

data Token
  = EmptyLine
  | Comment String
  | Label String
  | A AInstructionArg
  | C CInstructionArg

data AInstructionArg
  = SymbolA String
  | AddressA String

data CInstructionArg
  = DestCompJumpC String String String
  | DestCompC String String
  | CompJumpC String String

instance showAInstructionArg :: Show AInstructionArg where
  show (SymbolA x) = "(SymbolA " <> x <> ")"
  show (AddressA x) = "(AddressA " <> x <> ")"

instance showCInstructionArg :: Show CInstructionArg where
  show (DestCompJumpC d c j) = "(DestCompJumpC " <> d <> "=" <> c <> ";" <>j <> ")"
  show (DestCompC d c) = "(DestCompC " <> d <> "=" <> c <> ")"
  show (CompJumpC c j) = "(CompJumpC " <> c <> ";" <>j <> ")"

instance showToken :: Show Token where
  show EmptyLine = "(EmptyLine)"
  show (Comment s) = "(Comment " <> s <> ")"
  show (Label s) = "(Label " <> s <> ")"
  show (A arg) = "(A " <> show arg <> ")"
  show (C arg) = "(C " <> show arg <> ")"

newLine :: Parser String
newLine = singleton <$> S.char '\n'

empty :: Parser Token
empty = S.whiteSpace $> EmptyLine

comment :: Parser Token
comment = do
    _ <- S.string "//"
    Comment <$> foldl (<>) "" <$> C.many (singleton <$> S.anyChar)

symbolDeclaration :: Parser Token
symbolDeclaration = Label <$> (parseSymbolValue <* (comment <|> empty)) where
  contents :: Parser String
  contents = foldl (<>) "" <$> (C.many1 $ singleton <$> S.upperCaseChar)
  parseSymbolValue :: Parser String
  parseSymbolValue = C.between (S.string "(") (S.string ")") contents

aInstruction :: Parser Token
aInstruction = A <$> parse where
  parse :: Parser AInstructionArg
  parse = do
    let
      address = AddressA <$> foldl (<>) "" <$> C.many1 (singleton <$> S.anyDigit)
      symbol = SymbolA <$> foldl (<>) "" <$> C.many1 (singleton <$> S.anyLetter)
    _ <- S.string "@"
    address <|> symbol

cInstruction :: Parser Token
cInstruction = C <$> parse where
  dest :: Parser String
  dest = C.choice (S.string <$> ["M", "D", "MD", "A", "AM", "AD", "AMD"])
  jump :: Parser String
  jump = C.choice (S.string <$> ["JGT", "JEQ", "JGE", "JLT", "JNE", "JLE", "JMP"])
  comp :: Parser String
  comp = C.choice (S.string <$>
    ([ "D|M"
    , "D|A"
    , "D&M"
    , "D&A"
    , "M-D"
    , "A-D"
    , "D-M"
    , "D-A"
    , "D+M"
    , "D+A"
    , "M-1"
    , "A-1"
    , "D-1"
    , "M+1"
    , "A+1"
    , "D+1"
    , "-M"
    , "-A"
    , "-D"
    , "!M"
    , "!A"
    , "!D"
    , "M"
    , "A"
    , "D"
    , "-1"
    , "1"
    , "0"
    ]))
  destcompjump :: Parser CInstructionArg
  destcompjump = do
    d <- dest
    _ <- S.string "="
    c <- comp
    _ <- S.string ";"
    j <- jump
    pure $ DestCompJumpC d c j
  destcomp :: Parser CInstructionArg
  destcomp = do
    d <- dest
    _ <- S.string "="
    c <- comp
    pure $ DestCompC d c
  compjump :: Parser CInstructionArg
  compjump = do
    c <- comp
    _ <- S.string ";"
    j <- jump
    pure $ CompJumpC c j
  parse :: Parser CInstructionArg
  parse = try destcompjump <|> try compjump <|> destcomp

line :: Parser Token
line = C.choice [aInstruction, cInstruction, symbolDeclaration, comment, empty]

lines :: Parser (List String)
lines = C.sepEndBy1 (foldl (<>) "" <$> C.many (singleton <$> S.satisfy (_ /= '\n'))) newLine

parseLine :: LineContents -> Either ParseError Token
parseLine = runParser line

parse :: String -> Either ParseError (List Token)
parse src = runParser lines src >>= traverse parseLine

isASymbol :: Token -> Boolean
isASymbol (A (SymbolA _)) = true
isASymbol _ = false

value :: Token -> String
value (A (SymbolA i)) = i
value (A (AddressA i)) = i
value (Label i) = i
value _ = ""

aSymbols :: List Token -> List String
aSymbols tokens = value <$> filter isASymbol tokens

isUserVar :: String -> Boolean
isUserVar s = isRight (runParser userVar s) where
  userVar :: Parser (List Char)
  userVar = C.many1 S.lowerCaseChar

userVars :: List Token -> List String
userVars = filter isUserVar <<< aSymbols

isProgramInstruction :: Token -> Boolean
isProgramInstruction (A _) = true
isProgramInstruction (C _) = true
isProgramInstruction _ = false

program :: List Token -> List Token
program = filter isProgramInstruction

isLabel :: Token -> Boolean
isLabel (Label _) = true
isLabel _ = false

labelVars :: List Token -> List (Tuple String Int)
labelVars tks = (\(Tuple t i) -> Tuple (value t) i) <$> filter (isLabel <<< fst) programWithLabels where
  programOrLabel t = isProgramInstruction t || isLabel t
  build :: List Token -> Int -> List (Tuple Token Int) -> List (Tuple Token Int)
  build Nil count acc = acc
  build (h : t) count acc
    | isLabel h = build t (count) (Tuple h (count) : acc)
    | otherwise = build t (count + 1) (Tuple h (count) : acc)
  programWithLabels = reverse $ build (filter programOrLabel tks) 0 Nil
